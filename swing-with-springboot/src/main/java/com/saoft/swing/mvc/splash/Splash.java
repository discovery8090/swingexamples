/*
 * Created by JFormDesigner on Sun Sep 22 12:20:31 CST 2019
 */

package com.saoft.swing.mvc.splash;

import org.springframework.core.io.ClassPathResource;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.*;

/**
 * @author Brainrain
 */
public class Splash extends JFrame {
    public Splash() {
        //不显示边框
        setUndecorated(true);
        initComponents();
    }

    public JProgressBar getProgress() {
        return progress;
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        progress = new JProgressBar();
        imgPanel = new MyPanel();

        //======== this ========
        setMinimumSize(new Dimension(800, 600));
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());
        contentPane.add(progress, BorderLayout.SOUTH);

        //======== imgPanel ========
        {
            imgPanel.setMinimumSize(new Dimension(0, 0));
            imgPanel.setLayout(null);
        }
        contentPane.add(imgPanel, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JProgressBar progress;
    private JPanel imgPanel;
    // JFormDesigner - End of variables declaration  //GEN-END:variables

    private class MyPanel extends JPanel{
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            try {
                BufferedImage img = ImageIO.read(new ClassPathResource("/icon/fastdemo-splash.png").getURL());
                g.drawImage(img, 0, 0, getWidth(), getHeight(), this);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

}
