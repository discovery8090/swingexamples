package com.saoft.swing.mvc.main;

import com.saoft.swing.system.SpringContextHolder;
import org.springframework.stereotype.Component;

@Component("MainFrameController")
public class MainFrameController {

    private MainFrame mainFrame;

    /**
     * 准备工作 和显示主界面
     */
    public void prepareAndOpenFrame() {
        if(mainFrame==null){
            mainFrame = SpringContextHolder.getBean(MainFrame.class);
        }
        mainFrame.setVisible(true);
    }
}
